##########################################################
#
#
# aktana-learning Anchor estimates Aktana Learning Engines.
#
# description: process anchor result from calculateAnchor based on the mode specified
#
#
# created by : marc.cohen@aktana.com
# updated by : shirley.xu@aktana.com
#
# created on : 2015-10-27
# updated on : 2018-07-31
#
# Copyright AKTANA (c) 2015.
#
#
####################################################################################################################
library(futile.logger)
library(data.table)

processAnchorResult.Facility <- function(result, predictRundate, isNightly) {
  
  flog.debug("process anchor result dim=(%s) using Facility mode",paste(dim(result),collapse=","))
  
  # remove offset==0 (predictRundate) result
  resultProcessed <- result[result$date!=predictRundate,]
  # for facility mode, probability is the facility probablity, different for each facility
  setnames(resultProcessed,"facilityProb","probability")
  
  # split result to calendar and history result (it is possible one facility in both calendar and history)
  cal <- resultProcessed[resultProcessed$source=="calendar",]
  noCal <- resultProcessed[resultProcessed$source!="calendar",]
  # add probability for the same facilityId from different day cluster
  sumFacilityProb <- function(probability,latitude,longitude,accountId,source) {
    if ("calendar" %in% source) {
      ind <- which(source=="calendar")
      sumProbability <- min(1,sum(probability[ind], na.rm=TRUE))
      wsource <- "calendar"
    } else {
      sumProbability <- sum(probability, na.rm=TRUE)
      wsource <- source[1]
    }
    probability
    return (list(latitude=latitude[1], longitude=longitude[1], probability=sumProbability, accountId=accountId[1], source=wsource))
  }
  if (isNightly) {
    resultProcessed <- resultProcessed[,sumFacilityProb(probability,latitude,longitude,accountId,source),by=c("repId","date","facilityId")][,c("repId","date","latitude","longitude","probability","facilityId","accountId","source")]
  } else {
    resultProcessed <- resultProcessed[,sumFacilityProb(probability,latitude,longitude,accountId,source),by=c("repId","date","facilityId","source","accountId")][,c("repId","date","latitude","longitude","probability","facilityId","accountId","source")]
  }
  
  # add other needed columns
  resultProcessed$runDate <- predictRundate
  flog.debug("return from Facility Mode, dim(resultProcessed)=(%s)",paste(dim(resultProcessed),collapse=","))
  return(resultProcessed)
}

processAnchorResult.Centroid <- function(result, predictRundate) {
  
  flog.debug("process anchor result dim=(%s) using Centroid mode",paste(dim(result),collapse=","))
  
  # remove offset==0 (predictRundate) result
  resultProcessed <- result[result$date!=predictRundate,]
  # centroid mode use cluster probablity as probability
  setnames(resultProcessed,"clusterProb","probability")
  
  # function to calcualted weighted centroid and other info
  calculateWeightedCentroid <- function (latitude, longitude, facilityProb, maxNearDistance, maxFarDistance, probability, source) {
    wLatitude <- sum(latitude*facilityProb, na.rm=TRUE)/sum(facilityProb, na.rm=TRUE)
    wLongitude <- sum(longitude*facilityProb, na.rm=TRUE)/sum(facilityProb, na.rm=TRUE)
    if ("calendar" %in% source) {
      wsource <- 'calendar'
    } else {
      wsource <- source[1]
    }
    return (list(latitude=wLatitude, longitude=wLongitude, probability=probability[1],maxNearDistance=maxNearDistance[1], maxFarDistance=maxFarDistance[1], source=wsource))
  }
  # apply func
  resultProcessed <- resultProcessed[,calculateWeightedCentroid(latitude, longitude, facilityProb, maxNearDistance, maxFarDistance, probability, source), by=c("repId","date")]
  
  # add other needed columns
  resultProcessed$runDate <- predictRundate
  flog.debug("return from Centroid Mode, dim(resultProcessed)=(%s)",paste(dim(resultProcessed),collapse=","))
  return(resultProcessed)
}

####################################
### Main Func
####################################

processAnchorResult <- function(predictMode, result, predictRundate, isNightly) {
  
  flog.info("Start processAnchorResult with predictMode=%s, result dim=(%s)", predictMode, paste(dim(result),collapse=","))
  
  # set default value to resultProcessed
  flog.info("running predictModel=Centroid to process result to get default processed result")
  resultProcessedCentroid <- processAnchorResult.Centroid(result, predictRundate)
  resultProcessedFacility <- NULL
  
  # process result based different mode
  switch (
    predictMode,
    Centroid = {flog.info("predictModel=Centroid, centroid results are always outputed, no further processing;")},
    Facility = {flog.info("adding processed result from predictMode=Facility");resultProcessedFacility <- processAnchorResult.Facility(result, predictRundate, isNightly)},
    {flog.info("predictMode %s is not supported, output the default Centroid mode results",predictMode)}
  )
  
  resultProcessedList <- list(resultCentroid=resultProcessedCentroid,resultFacility=resultProcessedFacility)

  flog.info("Return from processAnchorResult with dim(resultProcessedCentroid)=(%s), dim(resultProcessedFacility)=(%s)", paste(dim(resultProcessedCentroid),collapse=","), ifelse(is.null(resultProcessedFacility),0,paste(dim(resultProcessedFacility),collapse=",")))
  return(resultProcessedList)
}