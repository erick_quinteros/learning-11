#########################################################
#
#
# aktana- TimeToEngage module.
#
# description: get TTE parameter into a list
#
# created by : wendong.zhu@aktana.com
#
# created on : 2019-02-19
#
# Copyright AKTANA (c) 2019.
#
#
####################################################################################################################

library(data.table)
library(futile.logger)

DAYS_PREDICT  <- 14   # 1 - defaul today; # 2 - today & tomorrow
LOOKBACK_MAX <- 730   # default max days back to check interactions events

################################################################
## function: retrieve TTE parameters
## Input:
##        config - the config for learning.properties structure
##        mtype  - "BUILD" or "SCORE"
## Return:
##        params list
################################################################
getTTEparams <- function(config, mtype)
{
    params <- list()

    params[["NUM_PARTITIONS"]] <- 96
    params[["NUM_PARTITIONS_W"]] <- 36

    params[["ProductInteractionTypes"]] <- c("SEND", "VISIT_DETAIL")  # pfizerus data

    allevents <- toupper(getConfigurationValueNew(config, "LE_MT_events")) # CONGRESSMTS;WEBINAR;
    EventTypes <- allevents
      
    if (mtype == "SCORE") {
        # for prediction, we need to remove target (RTE_OPEN, RTE_CLICK, etc)
        EventTypes <- EventTypes[EventTypes != "RTE_OPEN"]
        EventTypes <- EventTypes[EventTypes != "RTE_CLICK"]
    } else {  # BUILD
        EventTypes <- c(allevents, "RTE_OPEN", "RTE_CLICK") # appending "RTE_OPEN", "RTE_CLICK" which are target
    }
    EventTypes <- unique(EventTypes)  # remove any duplicated types

    params[["EventTypes"]] <- EventTypes

    segs <- getConfigurationValueNew(config, "LE_MT_segments")

    segNamesAPColMap <- list()
    if (length(segs)!=0) {
      # remove extra unnessary string <repAccountAttribute\:> added to learning.properties <LE_MS_addPredictorsFromAccountProduct> by DSE API & and save its original mapping for UI display
      segNamesAPColMap <- as.list(segs)
      segs <- gsub("repAccountAttribute\\:", "", segs, fixed=TRUE)
      segNamesAPColMap <- setNames(segNamesAPColMap, as.vector(segs))
      segNamesAPColMap <- list2env(segNamesAPColMap) # convert to hash for faster access
    }

    params[["segs"]] <- segs

    channels <- toupper(getConfigurationValueNew(config, "LE_MT_channels")) # channels to listen to 
    params[["channels"]] <- channels

    channelUID <- toupper(getConfigurationValueNew(config, "channelUID"))   # SEND_CHANNEL|VISIT_CHANNEL
    params[["channelUID"]] <- channelUID

    isReportOnly <- getConfigurationValueNew(config, "LE_MT_reportOnly", convertFunc=as.logical, defaultValue=FALSE)
    params[["isReportOnly"]] <- isReportOnly

    predictRunDate <- getConfigurationValueNew(config, "LE_MT_predictRunDate")  # this param is for QA only, which can be set to a past date.
                                                                        # default is not setting, which means today's date.
    if (predictRunDate == 0) {
        predictRunDate <- Sys.Date()
    } else {
        predictRunDate <- as.Date(predictRunDate)
    }
    params[["predictRunDate"]] <- predictRunDate
    flog.info("predictRunDate: %s", format(predictRunDate, "%Y-%m-%d"))

    prods <- getConfigurationValueNew(config, "LE_MT_addPredictorsFromAccountProduct")
    prods <- unique(prods)
    params[["prods"]] <- prods

    LOOKBACK <- min(getConfigurationValueNew(config, "LE_MT_lookBack"), LOOKBACK_MAX)
    params[["LOOKBACK"]] <- LOOKBACK

    params[["LOOKBACK_MAX"]] <- LOOKBACK_MAX

    predictAhead <- getConfigurationValueNew(config, "LE_MT_predictAhead")
    if (predictAhead == 0 || predictAhead == '' || is.na(predictAhead))
        predictAhead <- DAYS_PREDICT
    params[["predictAhead"]] <- predictAhead

    pNameUID <- getConfigurationValueNew(config, "productUID")
    params[["pNameUID"]] <- pNameUID

    useDeliveredEmail <- getConfigurationValueNew(config, "useDeliveredEmailOnly", convertFunc=as.logical, defaultValue=FALSE)
    params[["useDeliveredEmail"]] <- useDeliveredEmail

    target <- toupper(getConfigurationValueNew(config, "LE_MT_target"))
    params[["target"]] <- target

    GBMLearnRate <- getConfigurationValueNew(config, "LE_MT_GBMLearnRate")
    params[["GBMLearnRate"]] <- GBMLearnRate

    GBMntrees <- getConfigurationValueNew(config, "LE_MT_GBMntrees")
    params[["GBMntrees"]] <- GBMntrees

    RFtreeDepth <- getConfigurationValueNew(config, "LE_MT_RFtreeDepth")
    params[["RFtreeDepth"]] <- RFtreeDepth

    RFtreeNo <- getConfigurationValueNew(config, "LE_MT_RFtreeNo")
    params[["RFtreeNo"]] <- RFtreeNo

    AnalysisUseML <- getConfigurationValueNew(config, "LE_MT_AnalysisUseML")
    params[["AnalysisUseML"]] <- AnalysisUseML

    runStamp <- getConfigurationValueNew(config, "buildUID")
    params[["runStamp"]] <- runStamp

    params[["VAL_NO_DATA"]] <- 999

    return (params)
}
