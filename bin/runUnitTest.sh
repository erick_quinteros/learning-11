#!/bin/bash

# fucntion to read config
function readJson {
  UNAMESTR=`uname`
  if [[ "$UNAMESTR" == 'Linux' ]]; then
    SED_EXTENDED='-r'
  elif [[ "$UNAMESTR" == 'Darwin' ]]; then
    SED_EXTENDED='-E'
  fi;

  VALUE=`grep -m 1 "\"${2}\"" ${1} | sed ${SED_EXTENDED} 's/^ *//;s/.*: *"//;s/",?//'`

  if [ ! "$VALUE" ]; then
    #echo "Error: Cannot find \"${2}\" in ${1}, set to empty" >&2;
    echo '';
  else
    echo $VALUE ;
  fi;
}

# Set the learning home location if not set already (use absolute path)
if [ -z "$LEARNING_HOME" ] ; then
   PRG="$0"
   LEARNING_HOME=`dirname "$PRG"`/..
fi
HOME_DIR_PARAM='homedir=\"'"$LEARNING_HOME"'\"'

# read parameters from command line
while getopts m:f:h:s:u:w:p:e:c: name
do
  case $name in
    m)MODULE=$OPTARG;;
    f)FUNC=$OPTARG;;
    h)DB_HOST=$OPTARG;;
    s)DB_NAME=$OPTARG;;
    u)DB_USER=$OPTARG;;
    w)DB_PASSWORD=$OPTARG;;
    p)DB_PORT=$OPTARG;;
    e)EXTENTION=$OPTARG;; #If there are R and Py tests in Module, use -e to select which to run ("R" or "py")
    c)CS_DB_NAME=$OPTARG;;
    *)echo "Invalid arg";;
  esac
done

# get parameters from config file or set as default if not passed from command line

# If module is not defined.
if [ -z "$MODULE" ] ;
then
   echo "Error: MODULE (-m) is not defined correctly."
   exit 1
else
   TEST_DIR_PARAM='testdir=\"'"$MODULE/tests"'\"'
fi

# if func is not define, set to all as default
if [ -z "$FUNC" ] ; then
   FUNC="all"
fi
FUNC_PARAM='func=\"'"$FUNC"'\"'

# if no dbhost, dbusername, dbuser, dbname, dbport get from config
if [ -z "$DB_HOST" ] ; then
   DB_HOST=`readJson $LEARNING_HOME/common/unitTest/config.json dbhost`
fi
DB_HOST_PARAM='dbhost=\"'"$DB_HOST"'\"'

if [ -z "$DB_USER" ] ; then
   DB_USER=`readJson $LEARNING_HOME/common/unitTest/config.json dbuser`
fi
DB_USER_PARAM='dbuser=\"'"$DB_USER"'\"'

if [ -z "$DB_PASSWORD" ] ; then
   DB_PASSWORD=`readJson $LEARNING_HOME/common/unitTest/config.json dbpassword`
fi
DB_PASSWORD_PARAM='dbpassword=\"'"$DB_PASSWORD"'\"'

if [ -z "$DB_NAME" ] ; then
   DB_NAME=`readJson $LEARNING_HOME/common/unitTest/config.json dbname`
fi
DB_NAME_PARAM='dbname=\"'"$DB_NAME"'\"'

if [ -z "$DB_PORT" ] ; then
   DB_PORT=`readJson $LEARNING_HOME/common/unitTest/config.json dbport`
fi
if [ "$DB_PORT" == "" ] ; then
   DB_PORT_PARAM=''
else
   DB_PORT_PARAM='port="'"$DB_PORT"'"'
fi

CUSTOMER_PARAM='customer=\"'"unittest"'\"'

if [ -z "$CS_DB_NAME" ] ; then
   CS_DB_NAME="$DB_NAME""_cs"
fi
CS_DB_PARAM='dbname_cs=\"'"$CS_DB_NAME"'\"'

# # get additional parameters from test folder config file
# BUILD_UID=`readJson $LEARNING_HOME/$MODULE/tests/config.json buildUID`
# if [ "$BUILD_UID" == "" ]
# then
#    BUILD_UID_PARAM=''
# else
#    BUILD_UID_PARAM='BUILD_UID=\"'"$BUILD_UID"'\"'
# fi
#
# CONFIG_UID=`readJson $LEARNING_HOME/$MODULE/tests/config.json configUID`
# if [ "$CONFIG_UID" == "" ]
# then
#    CONFIG_UID_PARAM=''
# else
#    CONFIG_UID_PARAM='CONFIG_UID=\"'"$CONFIG_UID"'\"'
# fi
#
# RUN_UID=`readJson $LEARNING_HOME/$MODULE/tests/config.json runUID`
# if [ "$RUN_UID" == "" ]
# then
#    RUN_UID_PARAM=''
# else
#    RUN_UID_PARAM='RUN_UID=\"'"$RUN_UID"'\"'
# fi
#
# DEPLOY_ON_SUCCESS=`readJson $LEARNING_HOME/$MODULE/tests/config.json deployOnSuccess`
# if [ "$DEPLOY_ON_SUCCESS" == "" ]
# then
#    DEPLOY_ON_SUCCESS_PARAM=''
# else
#    DEPLOY_ON_SUCCESS_PARAM='deployOnSuccess=\"'"$DEPLOY_ON_SUCCESS"'\"'
# fi


RCMD="Rscript"
RSCRIPT="$LEARNING_HOME/common/unitTest/unitTestDriver.R"

PYCMD="python"
PYSCRIPT="$LEARNING_HOME/common/unitTest/unitTestDriver.py"

ARGS="$HOME_DIR_PARAM $TEST_DIR_PARAM $FUNC_PARAM \
        $DB_USER_PARAM $DB_PASSWORD_PARAM $DB_HOST_PARAM $DB_NAME_PARAM \
        $DB_PORT_PARAM $CUSTOMER_PARAM $CS_DB_PARAM"
        # \ $BUILD_UID_PARAM $CONFIG_UID_PARAM $RUN_UID_PARAM \
        # $DEPLOY_ON_SUCCESS_PARAM

RFULLCMD="$RCMD $RSCRIPT $ARGS"
PYFULLCMD="$PYCMD $PYSCRIPT $ARGS"

# Verify if the module is in place.
if [ ! -f "$RSCRIPT" -a ! -f "$PYSCRIPT" ] ; then
        echo "Error: Could not find $$RSCRIPT and $$PYSCRIPT"
        exit 1
elif [ ! -f "$RSCRIPT" ] ; then
        echo "Missing R unitTestDriver, running Python test"
        FULLCMD="$PYFULLCMD"
elif [ ! -f "$PYSCRIPT" ] ; then
        echo "Missing Python unitTestDriver, running R test"
        FULLCMD="$RFULLCMD"
else
        RTEST=$(find "$MODULE/tests/" -name "test*.r")
        PYTEST=$(find "$MODULE/tests/" -name "__init__.py")
        if [ -z "$RTEST" -a -z "$PYTEST" ] ; then
                  if [ "$EXTENTION" == "R" ] ; then
                            echo "Running R test"
                            FULLCMD="$RFULLCMD"
                  elif [ "$EXTENTION" == "py" ] ; then
                            echo "Running Python test"
                            FULLCMD="$PYFULLCMD"
                  else
                            echo "Error: Neither Python nor R scripts found in $TEST_DIR_PARAM"
                            exit 1
                  fi
        elif [ -z "$PYTEST" ] ; then
                  echo "Running R test"
                  FULLCMD="$RFULLCMD"
        elif [ -z "$RTEST" ] ; then
                  echo "Running Python test"
                  FULLCMD="$PYFULLCMD"
        else
                  FULLCMD="$RFULLCMD && $PYFULLCMD"
        fi
fi

echo "running unit test on $MODULE:func $FUNC"
echo $FULLCMD
eval $FULLCMD

if [ "$FULLCMD" == "$PYFULLCMD" ] ; then
        if [ "$FUNC" == "all" ] ; then
                echo "coverage run --source=$MODULE/ --omit=*tests/* $LEARNING_HOME/common/unitTest/unitTestDriver.py $ARGS && coverage report"
                eval "coverage run --source=$MODULE/ --omit=*tests/* $LEARNING_HOME/common/unitTest/unitTestDriver.py $ARGS && coverage report" 1>/dev/null 2>&1
                echo "coverage report"
                eval "coverage report"
                echo "coverage xml -i -o coverage-$MODULE.xml"
                eval "coverage xml -i -o coverage-$MODULE.xml"
                echo "mv coverage-$MODULE.xml" "/bin/coverage/"
                mv "coverage-$MODULE.xml" "bin/coverage/"
                echo "rm .coverage"
                rm ".coverage"

        else
                echo "coverage run --source=$MODULE.$FUNC --omit=*tests/* $LEARNING_HOME/common/unitTest/unitTestDriver.py $ARGS && coverage report" 1>/dev/null 2>&1
                eval "coverage run --source=$MODULE.$FUNC --omit=*tests/* $LEARNING_HOME/common/unitTest/unitTestDriver.py $ARGS"
                echo "coverage report"
                eval "coverage report"
                echo "coverage xml -i -o coverage-$MODULE.xml"
                eval "coverage xml -i -o coverage-$MODULE.xml"
                echo "mv coverage-$MODULE.xml" "bin/coverage/"
                mv "coverage-$MODULE.xml" "bin/coverage/"
                echo "rm .coverage"
                rm ".coverage"
        fi
fi


rc=$?
echo $rc

case $rc in
  0) echo "successfully running unit test on $MODULE:func $FUNC";;
  *) echo "failed running unit test on $MODULE:func $FUNC";;
esac

exit $rc
