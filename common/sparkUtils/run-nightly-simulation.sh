#!/usr/bin/env bash

# update for spark-submit to python3 instead of default python
export PYSPARK_PYTHON=python3
export PYSPARK_DRIVER_PYTHON=python3
# Export the SPARK_HOME and environment variables for Sparkling Water
export SPARK_HOME=/usr/lib/spark/
export HADOOP_CONF_DIR=/etc/hadoop/conf
export MASTER=yarn

# Current dir
TOPDIR="/learning/sparkling-water-2.3.27"

# MYSQL Driver JAR PATH
PARENT_DIR=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
MYSQL_JAR_PATH="$PARENT_DIR/jars/mysql-connector-java-8.0.15.jar"
echo "MySQL Jar Path = $MYSQL_JAR_PATH"

source "$TOPDIR/bin/sparkling-env.sh"
# Java check
checkJava
# Verify there is Spark installation
checkSparkHome
# Verify if correct Spark version is used
checkSparkVersion
# Check sparkling water assembly Jar exists
checkPyZipExists
# Check existence of dependant Python packages
# checkPythonPackages

SCRIPT_MASTER=${MASTER:-"$DEFAULT_MASTER"}
# SCRIPT_DEPLOY_MODE="cluster"
SCRIPT_DEPLOY_MODE=${DEPLOY_MODE:-"client"}
SCRIPT_DRIVER_MEMORY=40G
SCRIPT_H2O_SYS_OPS=${H2O_SYS_OPS:-""}
SCRIPT_EXECUTOR_MEMORY=50G

echo "---------"
echo "  Using master    (MASTER)       : $SCRIPT_MASTER"
echo "  Deploy mode     (DEPLOY_MODE)  : $SCRIPT_DEPLOY_MODE"
echo "  Driver memory   (DRIVER_MEMORY): $SCRIPT_DRIVER_MEMORY"
echo "  H2O JVM options (H2O_SYS_OPS)  : $SCRIPT_H2O_SYS_OPS"
echo "---------"
export SPARK_PRINT_LAUNCH_COMMAND=1
VERBOSE=--verbose

VERBOSE=
spark-submit \
--master "$SCRIPT_MASTER" \
--conf spark.dynamicAllocation.enabled=false \
--conf "spark.driver.extraJavaOptions=-XX:MaxPermSize=8192m" \
--conf "spark.executor.extraJavaOptions=-XX:MaxPermSize=8192m" \
--conf "spark.yarn.am.extraJavaOptions=-XX:MaxPermSize=8192m" \
--conf spark.driver.memoryOverhead=2048m \
--conf spark.executor.memoryOverhead=2048m \
--conf spark.yarn.am.memoryOverhead=2048m \
--jars "$MYSQL_JAR_PATH" \
--num-executors 2 \
--driver-memory "$SCRIPT_DRIVER_MEMORY" \
--executor-memory "$SCRIPT_EXECUTOR_MEMORY" \
--driver-java-options "$SCRIPT_H2O_SYS_OPS" \
--deploy-mode "$SCRIPT_DEPLOY_MODE" \
--py-files "$PY_ZIP_FILE" \
$VERBOSE \
"$@"
