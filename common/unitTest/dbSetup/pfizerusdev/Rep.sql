CREATE TABLE `Rep` (
  `repId` int(11) NOT NULL AUTO_INCREMENT,
  `repTypeId` int(11) NOT NULL,
  `workWeekId` int(11) NOT NULL,
  `externalId` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `repName` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `timeZoneId` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'IANA timeZone identifier for the rep',
  `isActivated` tinyint(1) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL,
  `seConfigId` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repId`),
  UNIQUE KEY `rep_uniqueAK` (`externalId`),
  KEY `rep_repTypeId` (`repTypeId`),
  KEY `rep_workWeekId` (`workWeekId`),
  KEY `rep_seConfigId` (`seConfigId`)
) ENGINE=InnoDB AUTO_INCREMENT=3730 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci