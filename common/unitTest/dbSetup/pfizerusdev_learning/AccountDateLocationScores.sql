CREATE TABLE `AccountDateLocationScores` (
  `accountId` int(11) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `dayOfWeek` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `periodOfDay` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `facilityDoWScore` double NOT NULL,
  `facilityPoDScore` double NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `runDate` date NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`accountId`, `facilityId`, `dayOfWeek`, `periodOfDay`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
