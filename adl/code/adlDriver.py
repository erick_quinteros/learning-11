from pyspark import SparkConf
from pyspark.context import SparkContext
from awsglue.context import GlueContext
import pandas as pd
#from delta.tables import *
from data_loading import Data_loading
from utils import Utils

pd.set_option('display.max_columns', 20)
pd.set_option('display.max_rows', 100)
import boto3
import os
from datetime import datetime
import json

if __name__ == '__main__':
    #  Getting Glue, Spark, Sql context and setting conf for delta
    now = datetime.now()
    date_time = now.strftime("%Y-%m-%d")
    print("The script is running on ", date_time)
    # create a glue Context and connect that with spark
    conf = SparkConf(loadDefaults=True)
    conf.set("spark.delta.logStore.class", "org.apache.spark.sql.delta.storage.S3SingleDriverLogStore")
    conf.set("spark.sql.caseSensitive", "true")
    conf.set("spark.sql.execution.arrow.enabled", "true")
    sc = SparkContext.getOrCreate()
    glueContext = GlueContext(sc)
    spark = glueContext.spark_session


    # get params from Config.json
    with open('config.json', 'r') as f:
        config = json.load(f)
    customer = config["customer"]
    s3_destination = config['s3_destination']
    s3_load_csv = config['s3_load_csv']
    s3_map_csv = config['s3_map_csv']
    source_s3_location = config['source_s3_location']
    environment = config['environment']
    archive_folder = config['archive_folder']
    tables = config['tables']
    bronze_tables = config['bronze_tables']

    utils = Utils(glueContext, customer)
    # version the data mapping and loading files
    df_load_pandas = utils.data_load_csv(glueContext, s3_load_csv)
    df_map_pandas = utils.data_load_csv(glueContext, s3_map_csv)
    df_map_spark = spark.createDataFrame(df_map_pandas)
    utils.df_to_delta(df_map_spark, s3_destination + "config/deltaMapping")
    print("The data mapping is versioned in Delta at "+s3_destination+"config/")
    df_load_spark = spark.createDataFrame(df_load_pandas)
    utils.df_to_delta(df_load_spark, s3_destination + "config/deltaLoading")
    print("The data loading is versioned in Delta at "+s3_destination+"config/")

    # # load data from rpt S3 source and version in destination S3
    # dataLoader = Data_loading(glueContext, customer,environment, df_load_pandas,df_map_pandas)
    # dataLoader.run(source_s3_location, s3_destination,archive_folder, bronze_tables, tables)



